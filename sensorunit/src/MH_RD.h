#include <Arduino.h>

class MH_RD
{
public:
    MH_RD(int8_t enabledPin, int8_t dataPin);
    bool readSensor();
};