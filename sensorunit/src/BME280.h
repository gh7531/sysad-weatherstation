#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <map>

class BME280: public Adafruit_BME280
{
public:
    std::map<String, float> readSensor();
};