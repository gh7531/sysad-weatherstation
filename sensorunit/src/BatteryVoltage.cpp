#include <Arduino.h>

class BatteryVoltage
{
private:
    int8_t dataPin_;
public:
    BatteryVoltage(int8_t dataPin);
    float readBattery();
};

BatteryVoltage::BatteryVoltage(int8_t dataPin)
{
    dataPin_ = dataPin;
    pinMode(dataPin_, INPUT);
}

float BatteryVoltage::readBattery(){
    float voltage;
    int16_t analogValue;
    analogValue = analogRead(dataPin_);
    // Derived from external voltage divider, R1=100k R2=240k
    voltage = analogValue * (3.3 / 1023.0) * (17.0/12.0);
    return voltage;
}