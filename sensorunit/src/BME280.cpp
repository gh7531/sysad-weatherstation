#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <map>

class BME280: public Adafruit_BME280
{   
public:
    std::map<String, float> readSensor();
};

std::map<String, float> BME280::readSensor() {
    std::map<String, float> measurements;

    float temperature = readTemperature();
    measurements.insert(std::make_pair("temperature", temperature));
    float humidity = readHumidity();
    measurements.insert(std::make_pair("humidity", humidity));
    float pressure = readPressure();
    measurements.insert(std::make_pair("pressure", pressure));

    return measurements;
}