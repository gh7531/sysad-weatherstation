#include <Arduino.h>

class BatteryVoltage
{
public:
    BatteryVoltage(int8_t dataPin);
    float readBattery();
};