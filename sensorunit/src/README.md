# Sensorunit

## Dependencies
- adafruit/Adafruit BME280 Library@^2.1.2

## WIFI

### Connect WIFI
To connect to wifi you must provide your wifi credentials in `/src/secrets.h`.  
If this file does not exist create it.
```
const char* SSID = "YOUR_SSID";
const char* PASSWORD = "YOUR_PASSWORD";
```
### Configure WIFI
Take a look at `/src/WIFIConfig.h` to see what can be configured.