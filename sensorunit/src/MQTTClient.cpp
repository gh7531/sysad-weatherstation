#include <PubSubClient.h>
#include <WiFiClient.h>

class MQTTClient: public PubSubClient
{
public:
    MQTTClient(Client& client): PubSubClient(client){};
    bool connect(String server, uint16_t port);
    bool sendMsg(String topic, String msg);
};

bool MQTTClient::connect(String server, uint16_t port) {
    setServer(server.c_str(), port);
    
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    
    return PubSubClient::connect(clientId.c_str());
}

bool MQTTClient::sendMsg(String topic, String msg) {
    return publish(topic.c_str(), msg.c_str());
}