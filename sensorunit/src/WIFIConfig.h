#include <ESP8266WiFi.h>
#include "secrets.h"
/**
 * const char* SSID = "YOUR_SSID";
 * const char* PASSWORD = "YOUR_PASSWORD";
 */
bool USE_DHCP = false; // Using DHCP may increase the time needed to establish a wifi connection
IPAddress LOCAL_IP(192, 168, 178, 10);
IPAddress GATEWAY(192, 168, 178, 1);
IPAddress SUBNET(255, 255, 255, 0);
IPAddress DNS1(192, 168, 178, 6);
IPAddress DNS2 = GATEWAY;