#include <Arduino.h>
#include <ESP8266WiFi.h>
#include "MQTTClient.h"
#include "WIFIConfig.h"
#include "BME280.h"
#include "MH_RD.h"
#include "BatteryVoltage.h"
#include <map>

#define MH_RD_ENABLE_PIN 13
#define MH_RD_DATA_PIN 14
#define BATTERY_DATA_PIN A0

#define SLEEP_DURATION 6*10e7 //-> 10min
#define MAX_CONNECT_RETRY 120

#define MQTT_BROKER "192.168.178.140"
#define MQTT_PORT 1883

WiFiClient wifiClient;
MQTTClient mqttClient(wifiClient);
BME280 bme280;
MH_RD mhrd(MH_RD_ENABLE_PIN, MH_RD_DATA_PIN);
BatteryVoltage battery(BATTERY_DATA_PIN);

bool setup_wifi() {
  WiFi.disconnect();
  Serial.print("Connecting to ");
  Serial.println(SSID);

  if (!USE_DHCP)
    if (WiFi.config(LOCAL_IP, GATEWAY, SUBNET, DNS1, DNS2))
      Serial.println("Config set successfully");
  WiFi.mode(WIFI_STA);

  WiFi.begin(SSID, PASSWORD);

  for (uint8_t i = 0; i < MAX_CONNECT_RETRY; i++)
  {
    if (WiFi.status() == WL_CONNECTED){
      Serial.println();
      Serial.println("WiFi connected");
      Serial.print("IP address: ");
      Serial.println(WiFi.localIP());
      Serial.print("Signal strength: ");
      Serial.println(WiFi.RSSI());
      return true;
    }
    delay(500);
    Serial.print(".");
  }
  return false;
}

void setup() {
  Serial.begin(9600);
  Serial.println("");

  int8_t status;
  status = bme280.begin(BME280_ADDRESS);

  if (!status)
  {
    status = bme280.begin(BME280_ADDRESS_ALTERNATE);
  }

  if (!status) {
    Serial.println("Could not find a valid BME280 sensor, check wiring, address, sensor ID!");
    Serial.print("SensorID was: 0x"); Serial.println(bme280.sensorID(),16);
    while (1) delay(10);
  }

  Serial.println("Found BME280");

  if (!setup_wifi()) 
  {
    Serial.println("Error connecting to WIFI");
    ESP.deepSleep(SLEEP_DURATION);
  }

  if(!mqttClient.connect(MQTT_BROKER, MQTT_PORT)) {
    Serial.println("Error connecting to MQTT");
    ESP.deepSleep(SLEEP_DURATION);
  }

  bool isRaining = mhrd.readSensor();
  Serial.print("isRaining: ");
  Serial.println(isRaining);

  std::map<String, float> measurements = bme280.readSensor();
  float temperature = measurements["temperature"];
  float humidity = measurements["humidity"];
  float pressure = measurements["pressure"] / 100;
  Serial.print("temperature: ");
  Serial.println(temperature);
  Serial.print("humidity: ");
  Serial.println(humidity);
  Serial.print("pressure: ");
  Serial.println(pressure);

  float voltage = battery.readBattery();
  Serial.print("voltage: ");
  Serial.println(voltage);

  int16_t signal_strength = WiFi.RSSI();

  String msg = "weather,sensorunit=1 temperature="
    + String(temperature, 2)
    + ",humidity="+ humidity
    + ",pressure="+ pressure
    + ",rainfall="+ isRaining
    + ",voltage="+ voltage
    + ",signalStrength="+ signal_strength;
  mqttClient.sendMsg("sensors", msg);
  mqttClient.disconnect();
      
  Serial.println("Good night!");
  ESP.deepSleep(SLEEP_DURATION);
}

void loop() {}
